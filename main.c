/**********************************************************************
 * Dual Playfield Demo
 * ROM Kernel Reference Manual - Libraries and Devices
 **********************************************************************/
#include <exec/types.h>
#include <hardware/dmabits.h>
#include <hardware/custom.h>
#include <hardware/cia.h>
#include <graphics/gfx.h>
#include <graphics/gfxbase.h>
#include <graphics/gfxmacros.h>
#include <graphics/rastport.h>
#include <graphics/view.h>
#include <exec/exec.h>

#include <clib/exec_protos.h>
#include <clib/dos_protos.h>
#include <clib/intuition_protos.h>
#include <clib/graphics_protos.h>

#include <stdio.h>
#include <stdlib.h>

#include "lib/iff.h"
#include <proto/iff.h>

#define DEPTH 3
#define WIDTH 320
#define HEIGHT 256
#define NOT_ENOUGH_MEMORY -1000

struct View v;
struct ViewPort vp1;
struct ViewPort vp2;
struct ViewPort vp3;
//struct BitMap b;

struct RasInfo ri1;
struct RasInfo ri2;
struct RasInfo ri3;
struct RasInfo ri4;
struct BitMap b2;

short i, j, k, n;
struct ColorMap *GetColorMap();
struct GfxBase *GfxBase;
struct IFFBase *IFFBase;
extern struct CIA ciaa, ciab;

/*
struct ColorMap *cm;
USHORT colortable[] = {
  0x000, 	// 0
  0x00f, 	// 1
  0x0f0, 	// 2 
  0x0ff, 	// 3
  0xfff,	// 4
  0xf00,	// 5
  0x0f0,  	// 6
  0xff0, 	// 7
  
  0xf0f, 	// 8
  0x004, 	// 9
  0x040, 	// 10 
  0x044, 	// 11
  0x444,	// 12
  0x400,	// 13
  0x040,  	// 14
  0x440, 	// 15
};
UWORD *colorpalette;
*/
struct RastPort rp, rp2;
struct View *oldview;

int rasScrollDir1 = 1;
int rasScrollDir2 = 1;
int rasScrollDir3 = 1;

int FreeMemory();
void Scroll(void);
int MouseClick(void);

struct BitmapData
{
	// Color data
	struct ColorMap* cm;
	UWORD colortable[32];
	LONG colorTableSize;
	
	// The bitmap data
	UBYTE planes;
	struct BitMap *bitmap;
	
	struct RasInfo rasInfo;
};

struct BitmapData *LoadBitmap(char *filename, struct RasInfo *next);

int main(int argc, char **argv)
{
	GfxBase = (struct GfxBase *) OpenLibrary("graphics.library", 0);
	if (GfxBase == NULL) exit(1);

	IFFBase = (struct IFFBase *)OpenLibrary("iff.library", 0);
	if (IFFBase == NULL) exit(1);
	
	InitView(&v);
	v.ViewPort = &vp1;
	
	InitVPort(&vp1);
	vp1.DWidth = WIDTH;
	vp1.DHeight = 60;
	vp1.RasInfo = &ri1;
	vp1.Modes = 0; //DUALPF | PFBA;
	vp1.Next = &vp2;
	
	InitVPort(&vp2);
	vp2.DWidth = WIDTH;
	vp2.DHeight = 60;
	vp2.DyOffset = 61;
	vp2.RasInfo = &ri2;
	vp2.Modes = 0;
	vp2.Next = &vp3;
	
	InitVPort(&vp3);
	vp3.DWidth = WIDTH;
	vp3.DHeight = 60;
	vp3.DyOffset = 122;
	vp3.RasInfo = &ri3;
	vp3.Modes = 0;
	vp3.Next = NULL;

	printf("InitBitMap 1\n");
	//InitBitMap(&b2, DEPTH, WIDTH, HEIGHT);
	struct BitmapData *bd1 = LoadBitmap("res/gfx/back1.iff", NULL);
	ri1.BitMap = bd1->bitmap;
	ri1.RxOffset = 0;
	ri1.RyOffset = 0;
	
	printf("InitBitMap 2\n");
	//InitBitMap(&b2, DEPTH, WIDTH, HEIGHT);
	struct BitmapData *bd2 = LoadBitmap("res/gfx/back2.iff", NULL);
	//ri1.Next = &ri2;
	ri2.BitMap = bd2->bitmap;
	ri2.RxOffset = 0;
	ri2.RyOffset = 0;
	
	printf("InitBitMap 3\n");
	//InitBitMap(&b2, DEPTH, WIDTH, HEIGHT);
	struct BitmapData *bd3 = LoadBitmap("res/gfx/back3.iff", NULL);
	//ri2.Next = &ri3;
	ri3.BitMap = bd3->bitmap;
	ri3.RxOffset = 0;
	ri3.RyOffset = 0;
	
	//printf("InitBitMap 4\n");
	//InitBitMap(&b2, DEPTH, WIDTH, HEIGHT);
	//struct BitmapData *bd4 = LoadBitmap("res/gfx/drawFront.iff", NULL);
	//ri3.Next = &ri4;
	//ri4.BitMap = bd4->bitmap;
	//ri4.RxOffset = 0;
	//ri4.RyOffset = 0;
	//ri4.Next = NULL;

	printf("GetColorMap\n");
	//cm = GetColorMap(16);
	//colorpalette = cm->ColorTable;
	//for (i = 0; i < 16; i++) *colorpalette++ = colortable[i];
	
	vp1.ColorMap = bd1->cm;
	LoadRGB4(&vp1, bd1->colortable, bd1->colorTableSize);
	
	vp2.ColorMap = bd2->cm;
	LoadRGB4(&vp2, bd2->colortable, bd2->colorTableSize);
	
	vp3.ColorMap = bd3->cm;
	LoadRGB4(&vp3, bd3->colortable, bd3->colorTableSize);

	/*
	printf("AllocRaster\n");
	for (i = 0; i < DEPTH; i++) {
		//b.Planes[i] = (PLANEPTR) AllocRaster(WIDTH, HEIGHT);
		//if (b.Planes[i] == NULL) exit(NOT_ENOUGH_MEMORY);
		//b2.Planes[i] = (PLANEPTR) AllocRaster(WIDTH, HEIGHT);
		//if (b2.Planes[i] == NULL) exit(NOT_ENOUGH_MEMORY);
	}*/
	
	printf("InitRastPort\n");
	//InitRastPort(&rp);
	//InitRastPort(&rp2);

	printf("SetBitMap\n");
	//rp.BitMap = bd1->bitmap;
	//rp2.BitMap = &b2;

	printf("MakeVPort\n");
	MakeVPort(&v, &vp1);
	MakeVPort(&v, &vp2);
	MakeVPort(&v, &vp3);

	printf("MrgCop\n");
	MrgCop(&v);

	//SetRast(&rp, 0);
	//SetRast(&rp2, 0);

	printf("LoadView");
	oldview = GfxBase->ActiView;
	LoadView(&v);

	/*
	printf("DrawShit\n");
	int x = 10;
	int y = 10;
	for (i = 0; i < 4; i++)
	{
		//SetAPen(&rp, i+1);
		//RectFill(&rp, x, y, x+150, y+100);
		
		x+=10;
		y+=10;
		
		//SetAPen(&rp2, i+1);
		//RectFill(&rp2, x, y, x+150, y+100);
		
		x+=10;
		y+=10;
	}

	//printf("PokeShit\n");
	//SetAPen(&rp2, 0);
	//RectFill(&rp2, 110, 15, 130, 175);
	//RectFill(&rp2, 175, 15, 200, 175);
	*/
	
	printf("Wait\n");
    while (!MouseClick())
    {
		//Scroll();
		WaitTOF();
    }
	
	printf("Unload");
	LoadView(oldview);
	WaitTOF();
	FreeMemory();
	
	CloseLibrary((struct Library *) GfxBase);
	CloseLibrary((struct Library *) IFFBase);

	return 0;
}


void Scroll(void)
{
	// 1
	if(ri1.RxOffset < 0)
	{
		rasScrollDir1 = 1;	
	}
	
	if(ri1.RxOffset > 50)
	{
		rasScrollDir1 = -1;
	}
	
	ri1.RxOffset+=rasScrollDir1;
	
	// 2
	if(ri2.RxOffset < 0)
	{
		rasScrollDir2 = 2;	
	}
	
	if(ri2.RxOffset > 50)
	{
		rasScrollDir2 = -2;
	}
	
	ri2.RxOffset+=rasScrollDir1;
	
	// 3
	if(ri3.RxOffset < 0)
	{
		rasScrollDir3 = 3;	
	}
	
	if(ri3.RxOffset > 50)
	{
		rasScrollDir3 = -3;
	}
	
	ri3.RxOffset+=rasScrollDir3;
	
	ScrollVPort(&vp1);
	ScrollVPort(&vp2);
	ScrollVPort(&vp3);
}

int FreeMemory()
{
  for (i = 0; i < DEPTH; i++) {
    //FreeRaster(b.Planes[i], WIDTH, HEIGHT);
    FreeRaster(b2.Planes[i], WIDTH, HEIGHT);
  }
  
  //FreeColorMap(cm);
  FreeVPortCopLists(&vp1);
  FreeVPortCopLists(&vp2);
  FreeVPortCopLists(&vp3);
  FreeCprList(v.LOFCprList);
  return 0;
}

int MouseClick(void)
{
    return !(ciaa.ciapra & CIAF_GAMEPORT0);
}

struct BitmapData *LoadBitmap(char *filename, struct RasInfo *next)
{	
	struct BitmapData *bitmapData = AllocMem(sizeof(struct BitmapData), MEMF_CHIP | MEMF_CLEAR);
	
	// Load background into bitmap
	printf("Load bitmap: %s\n",filename);
	IFFL_HANDLE iff = IFFL_OpenIFF(filename, IFFL_MODE_READ);
	if(iff == NULL)
	{
        printf("IFF open failed :(\n");
        return FALSE;
	}		

	struct IFFL_BMHD *bmhd = IFFL_GetBMHD(iff);
	if(bmhd==NULL)
	{
		printf("IFF is not bitmap :(\n");
        return FALSE;
	}
	
	bitmapData->cm = GetColorMap(16);
	if (bitmapData->cm == NULL)
	{
		printf("Could not get ColorMap\n");
        return FALSE;
	}
	
	// Load the background colour map
	bitmapData->colorTableSize = IFFL_GetColorTab(iff, bitmapData->colortable);	
	
	// Allocate the bitmap and decode it
	bitmapData->planes = bmhd->nPlanes;
	printf("Btmap: %dx%d @ %d planes\n",bmhd->w, bmhd->h, bmhd->nPlanes);
	bitmapData->bitmap = AllocBitMap(bmhd->w, bmhd->h, bmhd->nPlanes, BMF_CLEAR | BMF_DISPLAYABLE, NULL);
	if(!IFFL_DecodePic(iff, bitmapData->bitmap))
	{
		printf("Could not decode map bitmap\n");
        return FALSE;
	}
	
	bitmapData->rasInfo.BitMap = bitmapData->bitmap;
	bitmapData->rasInfo.RxOffset = 0;
	bitmapData->rasInfo.RyOffset = 0;
	bitmapData->rasInfo.Next = next;
	
	return bitmapData;
}