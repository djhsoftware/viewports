CC=vc
LDFLAGS=-lamiga -lauto
INC=-Iinclude
EXE=bin/viewports
SRC=main.c
	
UAE=D:\\media\\Software\\Emulation\\Amiga\\Harddrives\Applications\\dev\\viewports\\

all: 
	@rmdir /s/q bin
	@mkdir bin\res
	$(CC) $(SRC) $(INC) $(LDFLAGS) -o $(EXE)	
	@rmdir /s/q $(UAE)
	@mkdir $(UAE)
	@xcopy /S/Q/Y res\\* bin\\res
	@xcopy /S/Q/Y bin\\* $(UAE)
